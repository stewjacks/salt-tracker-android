package com.salientsalt.util;

/**
 * Created by Stewart on 2015-10-28.
 *
 * borrowed from vyshane/rex-weather
 */

public class TemperatureFormatter {

	public static String format(float temperature) {
		return String.valueOf(Math.round(temperature)) + "°";
	}
}
