package com.salientsalt.views;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.salientsalt.NewTripTestActivity;
import com.salientsalt.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;

public class NewEntryLocationFragment extends BaseFragment implements OnMapReadyCallback {

	Subscriber<Location> subscriber = new Subscriber<Location>() {
		@Override
		public void onCompleted() {}

		@Override
		public void onError(Throwable e) {}

		@Override
		public void onNext(Location location) {
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15f));
		}
	};
	// Get our current location.

	@Bind(R.id.map_view)
	MapView mMapView;

	private GoogleMap mMap;
	private Location mLastLocation;


	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
							 final Bundle savedInstanceState) {

		// Inflate the layout for this fragment

		View v = inflater.inflate(R.layout.fragment_new_entry_location, container, false);

		ButterKnife.bind(this, v);
		mMapView.onCreate(savedInstanceState);
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		mMapView.onPause();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		mMapView.onDestroy();
		super.onDestroy();
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS) {
			MapsInitializer.initialize(getActivity());
			mMapView.getMapAsync(this);
		}

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		mMap.setMyLocationEnabled(true);
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		mMap.setMyLocationEnabled(true);
		centerMapOnMyLocation();
	}

	public void setLastLocation(Location location) {
		mLastLocation = location;
		centerMapOnMyLocation();
	}


	private void centerMapOnMyLocation() {

		NewTripTestActivity activity = (NewTripTestActivity) getActivity();

		if (mLastLocation != null) {
			LatLng myLocation = new LatLng(mLastLocation.getLatitude(),
					mLastLocation.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
					15f));
		}

	}
}
