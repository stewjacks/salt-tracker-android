package com.salientsalt.views;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salientsalt.R;
import com.salientsalt.models.CurrentWeather;
import com.salientsalt.models.WeatherForecast;
import com.salientsalt.services.LocationService;
import com.salientsalt.services.WeatherService;
import com.salientsalt.util.DayFormatter;
import com.salientsalt.util.TemperatureFormatter;

import org.apache.http.HttpException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class NewEntryWeatherFragment extends BaseFragment {

    @Bind(R.id.temperature_text) TextView mCurrentTemperatureTextView;
    @Bind(R.id.location_text) TextView mLocationNameTextView;
    @Bind(R.id.weather_listview) ListView mForecastListView;
    @Bind(R.id.weather_progress) ProgressBar mProgressBar;

    private CompositeSubscription mCompositeSubscription;

    private static final String KEY_CURRENT_WEATHER = "key_current_weather";
    private static final String KEY_WEATHER_FORECASTS = "key_weather_forecasts";
    private static final long LOCATION_TIMEOUT_SECONDS = 20;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        // Inflate the layout for this fragment

		View v = inflater.inflate(R.layout.fragment_new_entry_weather, container, false);

		ButterKnife.bind(this, v);

        mCompositeSubscription = new CompositeSubscription();

        final WeatherForecastListAdapter adapter = new WeatherForecastListAdapter(
				new ArrayList<WeatherForecast>(), getActivity());
		mForecastListView.setAdapter(adapter);

        updateWeather();

        return v;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Provides items for our list view.
     */
    private class WeatherForecastListAdapter extends ArrayAdapter {

        public WeatherForecastListAdapter(final List<WeatherForecast> weatherForecasts,
                                          final Context context) {
            super(context, 0, weatherForecasts);
        }

        @Override
        public boolean isEnabled(final int position) {
            return false;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                final LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                convertView = layoutInflater.inflate(R.layout.weather_forecast_list_item, null);

                viewHolder = new ViewHolder();
                viewHolder.dayTextView = (TextView) convertView.findViewById(R.id.day);
                viewHolder.descriptionTextView = (TextView) convertView
                        .findViewById(R.id.description);

                viewHolder.rainTextView = (TextView) convertView.findViewById(R.id.rain);

                viewHolder.snowTextView = (TextView) convertView.findViewById(R.id.snow);

                viewHolder.maximumTemperatureTextView = (TextView) convertView
                        .findViewById(R.id.maximum_temperature);
                viewHolder.minimumTemperatureTextView = (TextView) convertView
                        .findViewById(R.id.minimum_temperature);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final WeatherForecast weatherForecast = (WeatherForecast) getItem(position);

            final DayFormatter dayFormatter = new DayFormatter(getContext());
            final String day = dayFormatter.format(weatherForecast.getTimestamp());
            viewHolder.dayTextView.setText(day);
            viewHolder.descriptionTextView.setText(weatherForecast.getDescription());
            viewHolder.rainTextView.setText(String.format("%.2f", weatherForecast.getRain()));
            viewHolder.snowTextView.setText(String.format("%.2f", weatherForecast.getSnow()));
            viewHolder.maximumTemperatureTextView.setText(
                    TemperatureFormatter.format(weatherForecast.getMaximumTemperature()));
            viewHolder.minimumTemperatureTextView.setText(
                    TemperatureFormatter.format(weatherForecast.getMinimumTemperature()));

            return convertView;
        }

        /**
         * Cache to avoid doing expensive findViewById() calls for each getView().
         */
        private class ViewHolder {
            private TextView dayTextView;
            private TextView descriptionTextView;
            private TextView rainTextView;
            private TextView snowTextView;
            private TextView maximumTemperatureTextView;
            private TextView minimumTemperatureTextView;
        }
    }

    /**
     * Get weather data for the current location and update the UI.
     */
    private void updateWeather() {

        final LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        final LocationService locationService = new LocationService(locationManager);

        // Get our current location.
        final Observable fetchDataObservable = locationService.getCoarseLocation()
                .timeout(LOCATION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .flatMap(   new Func1<Location, Observable<HashMap<String, WeatherForecast>>>() {
                    @Override
                    public Observable<HashMap<String, WeatherForecast>> call(final Location location) {
                        final WeatherService weatherService = new WeatherService();
                        final double longitude = location.getLongitude();
                        final double latitude = location.getLatitude();

                        return Observable.zip(
                                // Fetch current and 7 day forecasts for the location.
                                weatherService.fetchCurrentWeather(longitude, latitude),
                                weatherService.fetchWeatherForecasts(longitude, latitude),

                                // Only handle the fetched results when both sets are available.

                                new Func2<CurrentWeather, List<WeatherForecast>,
                                        HashMap<String, WeatherForecast>>() {

                                    @Override
                                    public HashMap call(final CurrentWeather currentWeather,
                                                        final List<WeatherForecast> weatherForecasts) {

                                        HashMap weatherData = new HashMap();
                                        weatherData.put(KEY_CURRENT_WEATHER, currentWeather);
                                        weatherData.put(KEY_WEATHER_FORECASTS, weatherForecasts);
                                        return weatherData;
                                    }
                                }
                        );
                    }
                });

        mCompositeSubscription.add(fetchDataObservable
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<HashMap<String, WeatherForecast>>() {
                                       @Override
                                       public void onNext(final HashMap<String, WeatherForecast> weatherData) {
                                           // Update UI with current weather.
                                           mProgressBar.setVisibility(View.GONE);
										   final CurrentWeather currentWeather = (CurrentWeather) weatherData
												   .get(KEY_CURRENT_WEATHER);
										   mLocationNameTextView.setText(currentWeather.getLocationName());
										   mCurrentTemperatureTextView.setText(
												   TemperatureFormatter.format(currentWeather.getTemperature()));

										   // Update weather forecast list.
										   final List<WeatherForecast> weatherForecasts = (List<WeatherForecast>)
												   weatherData.get(KEY_WEATHER_FORECASTS);
										   final WeatherForecastListAdapter adapter = (WeatherForecastListAdapter)
												   mForecastListView.getAdapter();

										   adapter.clear();
										   adapter.addAll(weatherForecasts);

                                       }

                                       @Override
                                       public void onCompleted () {
//								mSwipeRefreshLayout.setRefreshing(false);
                                       }

                                       @Override
                                       public void onError ( final Throwable error){
//								mSwipeRefreshLayout.setRefreshing(false);

                                           if (error instanceof TimeoutException) {
                                               Toast.makeText(getActivity(), R.string.error_location_unavailable, Toast.LENGTH_LONG).show();
                                           } else if (error instanceof RetrofitError
                                                   || error instanceof HttpException) {
                                               Toast.makeText(getActivity(), R.string.error_fetch_weather, Toast.LENGTH_LONG).show();
                                           } else {
                                               Log.e("Salt", error.getMessage());
                                               error.printStackTrace();
                                               throw new RuntimeException("See inner exception");
                                           }
                                       }
                                   }
                        )
        );
    }
}
