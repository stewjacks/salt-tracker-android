package com.salientsalt.authentication;

import com.amazonaws.auth.AWSAbstractCognitoDeveloperIdentityProvider;
import com.amazonaws.regions.Regions;

/**
 * Created by Stewart on 2015-10-24.
 */
public class SaltAuthenticationProvider extends AWSAbstractCognitoDeveloperIdentityProvider {
	private static final String sDeveloperProvider = "<Developer_provider_name>";

	public SaltAuthenticationProvider(String accountId, String identityPoolId, Regions region) {
		super(accountId, identityPoolId, region);
	}

	@Override
	public String getProviderName() {
		return sDeveloperProvider;
	}

	@Override
	public String refresh() {

		// Override the existing token
		setToken(null);

		// Get the identityId and token by making a call to your backend
		// (Call to your backend)

		// Call the update method with updated identityId and token to make sure
		// these are ready to be used from Credentials Provider.

		update(identityId, token);
		return token;

	}

	// If the app has a valid identityId return it, otherwise get a valid
	// identityId from your backend.

	@Override
	public String getIdentityId() {

		// Load the identityId from the cache
//		identityId = cachedIdentityId;

		if (identityId == null) {
			// Call to your backend
			return "";
//			return string;
		} else {
			return identityId;
		}
	}
}
