package com.salientsalt.models;

/**
 * Created by Stewart on 2015-10-28.
 *
 * borrowed from vyshane/rex-weather
 */
public class WeatherForecast {
	private final String mLocationName;
	private final long mTimestamp;
	private final String mDescription;
	private final float mMinimumTemperature;
	private final float mMaximumTemperature;
	private final float mRain;
	private final float mSnow;

	public WeatherForecast(final String locationName,
						   final long timestamp,
						   final String description,
						   final float rain,
						   final float snow,
						   final float minimumTemperature,
						   final float maximumTemperature) {

		mLocationName = locationName;
		mTimestamp = timestamp;
		mRain = rain;
		mSnow = snow;
		mMinimumTemperature = minimumTemperature;
		mMaximumTemperature = maximumTemperature;
		mDescription = description;
	}

	public String getLocationName() {
		return mLocationName;
	}

	public long getTimestamp() {
		return mTimestamp;
	}

	public String getDescription() {
		return mDescription;
	}

	public float getRain() {
		return mRain;
	}

	public float getSnow() {
		return mSnow;
	}

	public float getMinimumTemperature() {
		return mMinimumTemperature;
	}

	public float getMaximumTemperature() {
		return mMaximumTemperature;
	}
}