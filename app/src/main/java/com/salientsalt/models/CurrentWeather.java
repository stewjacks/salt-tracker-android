package com.salientsalt.models;

/**
 * Created by Stewart on 2015-10-28.
 *
 * borrowed from vyshane/rex-weather
 */
public class CurrentWeather extends WeatherForecast {
	private final float mTemperature;  // Current temperature.

	public CurrentWeather(final String locationName,
						  final long timestamp,
						  final String description,
						  final float temperature,
						  final float rain,
						  final float snow,
						  final float minimumTemperature,
						  final float maximumTemperature) {

		super(locationName, timestamp, description, rain, snow, minimumTemperature, maximumTemperature);
		mTemperature = temperature;
	}

	public float getTemperature() {
		return mTemperature;
	}
}
